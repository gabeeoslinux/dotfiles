# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "hawk"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Argentina/Buenos_Aires";

  # Select internationalisation properties.
  i18n.defaultLocale = "es_AR.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "es_AR.UTF-8";
    LC_IDENTIFICATION = "es_AR.UTF-8";
    LC_MEASUREMENT = "es_AR.UTF-8";
    LC_MONETARY = "es_AR.UTF-8";
    LC_NAME = "es_AR.UTF-8";
    LC_NUMERIC = "es_AR.UTF-8";
    LC_PAPER = "es_AR.UTF-8";
    LC_TELEPHONE = "es_AR.UTF-8";
    LC_TIME = "es_AR.UTF-8";
  };
  # Graphic Environment
  programs.hyprland.enable = true;

  programs.waybar.package = {
  enable = true;
  package = pkgs.waybar.overrideAttrs (oldAttrs: {
  mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
  });
  };

  programs.hyprland.xwayland = {
  enable = true;
  }; 
  
  # Audio
  security.rtkit.enable = true;
  sound.enable = true;
  services.pipewire = {
  enable = true;
  alsa.enable = true;
  pulse.enable = true;
  
  };

  # gvfs for easy workflows with external drives
  services.gvfs = {
  enable = true;
  };
 
 # Tumbler - Thumbnail support for images
  services.tumbler = {
  enable = true;  
  };

  # Configure keymap in X11
  services.xserver = {
    layout = "latam";
    xkbVariant = "";
  };

  # Configure console keymap
  console.keyMap = "la-latin1";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.gabe = {
    isNormalUser = true;
    description = "gabe";
    extraGroups = [ "networkmanager" "wheel" ];
    shell = pkgs.zsh;
    packages = with pkgs; [];
  };

  # GTK Settings themes
#    gtk.iconTheme.package = catppuccin-papirus-folders.overide {
#    flavor = "mocha";
#    accents = "red";
#    };
#    name = "Catppuccin-Papirus-Folders";
#   };
#  };

# Inspired by: https://github.com/divnix/digga/blob/4ebf259d11930774b3a13b370b955a8765bfcae6/configuration.nix#L30
nixpkgs.overlays = let
    overlays = map (name: import (./overlays + "/${name}"))
		(builtins.attrNames (builtins.readDir ./overlays));
	in overlays;


                     	  

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    git
    firefox-wayland
    mpv
    ncmpcpp
    pavucontrol
    pipewire
    kitty
    neofetch
    duf
    btop
    kate
    killall
    ranger
    viewnior
    brightnessctl
    pamixer
    brillo
    gimp
    jp2a
    scrot
    unzip
    ark
    p7zip
    wireplumber
    oh-my-zsh
    eza
    colord
    ffmpegthumbnailer
    gnome.gnome-keyring
    grimblast
    gtk-engine-murrine
    imagemagick
    playerctl
    meson
    libsForQt5.polkit-kde-agent
    libsForQt5.qt5.qtquickcontrols
    libsForQt5.qt5.qtquickcontrols2
    libsForQt5.qtstyleplugin-kvantum
    qt6Packages.qtstyleplugin-kvantum
    catppuccin-kvantum
    dbus
  #  catppuccin-gtk
  #  catppucin-papirus-folders.override { colorVariants = [ "pink" ]; }
  #  papirus-icon-theme
    gtk3
    qt5.qtwayland
    qt6.qtwayland
    qt6.qmake
    swww
    gvfs
    xfce.tumbler
    xdg-desktop-portal-hyprland
    xdg-desktop-portal-gtk
    xdg-user-dirs
    xdotool
    fnm
    xwayland
    cliphist
    libsForQt5.qt5ct
    eww-wayland
    nwg-look
    hyprland
    hyprpaper
    hyprpicker
    rofi-wayland
    swaylock-effects
    waybar
    wl-color-picker
    wofi
    wlroots
    wlogout
    xfce.xfce4-settings    

  ];
  # Nerd Fonts
  fonts.fontDir.enable = true;
  fonts.fonts = with pkgs; [
  nerdfonts
  iosevka
  font-awesome
  ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
    programs = {
    zsh = {
    enable = true;
    autosuggestions.enable = true;
    syntaxHighlighting.enable = true;
    ohMyZsh = {
    enable = true;
    theme = "intheloop";
    plugins = [
    "git"
    ];
    };
    };
    };

  # Thunar
  programs.thunar.enable = true;
  programs.xfconf.enable = true;

  # Thunar plugins
  programs.thunar.plugins = with pkgs.xfce; [
  thunar-archive-plugin
  thunar-volman
  thunar-media-tags-plugin
  ];

 
    


  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.dbus.enable = true;
    xdg.portal = {
        enable = true;
        extraPortals = [ 
        pkgs.xdg-desktop-portal-gtk
        ];
    };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

# Automatic Garbage Colletions
nix.gc = {
               automatic = true;
               dates = "weekly";
               options = "--delete-older-than 7d";
          };

}
