# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;

    boot.loader.grub.enable = true;
    boot.loader.grub.device = "nodev";
    boot.loader.grub.efiSupport = true;
    boot.loader.efi.canTouchEfiVariables = true;
    boot.loader.efi.efiSysMountPoint = "/boot";
    boot.loader.grub.useOSProber = true;

  networking.hostName = "hawk"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "America/Buenos_Aires";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "es_AR.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "es_AR.UTF-8";
    LC_IDENTIFICATION = "es_AR.UTF-8";
    LC_MEASUREMENT = "es_AR.UTF-8";
    LC_MONETARY = "es_AR.UTF-8";
    LC_NAME = "es_AR.UTF-8";
    LC_NUMERIC = "es_AR.UTF-8";
    LC_PAPER = "es_AR.UTF-8";
    LC_TELEPHONE = "es_AR.UTF-8";
    LC_TIME = "es_AR.UTF-8";
};
    
  # Configure console keymap
  console = {
  #   font = "Lat2-Terminus16";
     keyMap = "la-latin1";
  #   useXkbConfig = true; # use xkb.options in tty.
   };

  # Enable the X11 windowing system.
  #  services.xserver.enable = true;
  
  # Enable the i3-wm Environment
  #  service.xserver.windowManager.i3.enable = true;
    services.xserver = {
      enable = true;
      #displayManager.lightdm.enable = true;
      #windowManager.i3.enable = true;
      #windowManager.qtile.enable = true;
      displayManager.sddm = {
      enable = true;
      theme = "maya";
      autoNumlock = true;
    };
    };
      
    services.picom = {
    enable = true;
    fade = true;
    inactiveOpacity = 0.9;
    shadow = false;
    fadeDelta = 4;
    };

   # Picom Settings
   services.picom.settings = {
   corner-radius = 0.8;
   round-borders = 1;
   };

  
   # gvfs for easy workflows with external drives
    services.gvfs = {
    enable = true;
    };


  # Configure keymap in X11
   services.xserver.xkb.layout = "latam";
  # services.xserver.xkb.options = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
   sound.enable = true;
  # hardware.pulseaudio.enable = true;
  
  services.pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
        jack.enable = true;
        wireplumber.enable = true;
    };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
   users.users.gabe = {
     isNormalUser = true;
     extraGroups = [ "networkmanager" "wheel" ]; # Enable ‘sudo’ for the user.
     shell = pkgs.zsh;
  #   packages = with pkgs; [
  #     firefox
  #     tree
  #   ];
   };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
   environment.systemPackages = with pkgs; [
            kitty
            brightnessctl
            brillo
            cargo
            cmake
            dracula-theme
            emacs
            eww-wayland
            firefox-wayland
            flameshot
            fontpreview
            fzf
            gcc
            gcolor2
            gimp
            git
            glibc
            gnome.gnome-keyring
            gnumake
            go
            gtk3
            hplip
            hyprland
            hyprpaper
            hyprpicker
            jp2a
            libsecret
            libvirt
            lsd
            lxappearance
            meson
            mpv
            neofetch
            ninja
            networkmanagerapplet
            pavucontrol
            pipewire
            pkg-config
            polkit_gnome
            qt5.qtwayland
            qt6.qmake
            qt6.qtwayland
            ranger
            ripgrep
            rofi-wayland
            rustup
            scrot
            sddm
            shellcheck
            silver-searcher
            simplescreenrecorder
            tldr
            trash-cli
            unzip
            virt-viewer
            waybar
            wget
            wireplumber
            wl-color-picker
            wofi
            wlroots
            xdg-desktop-portal-hyprland
            xdg-desktop-portal-gtk
            xdg-utils
            xwayland
            ydotool
            zoxide
            oh-my-zsh
            eza
     
   ];

      fonts.fontDir.enable = true;
      fonts.fonts = with pkgs; [
         nerdfonts
         iosevka
    ];
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
     enable = true;
     enableSSHSupport = true;
   };

   services.dbus.enable = true;
    xdg.portal = {
        enable = true;
        extraPortals = [ 
        pkgs.xdg-desktop-portal-gtk
        ];
    };
     

    programs = {
      zsh = {
        enable = true;
        autosuggestions.enable = true;
        syntaxHighlighting.enable = true;
        ohMyZsh = {
          enable = true;
          theme = "agnoster";
          plugins = [
            "git"
          ];
        };
      };
    };   

 # Thunar
 programs.thunar.enable = true;
 programs.xfconf.enable = true;
 
 # Thunar plugins
 programs.thunar.plugins = with pkgs.xfce; [
 thunar-archive-plugin
 thunar-volman
 thunar-media-tags-plugin
 ];  

 programs.hyprland = {
 enable = true;
 };

 programs.waybar.package = {
 enable = true;
 package = pkgs.waybar.overrideAttrs (oldAttrs: {
 mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
 });
 };

 programs.hyprland.xwayland = {
 enable = true;
 };  

  # List services that you want to enable:

  services.locate = {
  enable = true;
  locate = pkgs.mlocate;
  };

  # environment etc
  environment.etc = {
     "xdg/gtk-3.0" .source = ./gtk-3.0;
    };

  
  # Enable unfree packages
    nixpkgs.config.allowUnfree = true;
  
  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

